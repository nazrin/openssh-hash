# OpenSSH-hash
Simple [FFI](https://luajit.org/ext_ffi.html "FFI") binding to OpenSSH hashes, MD5, SHA256, SHA224, SHA512, etc

# API

* `Cipher       lib.cipher(id)` - `MD5`, `SHA256`, `SHA224`, `SHA512`, `whirlpool`, etc
* `Cipher       Cipher:update(str, ...)` - One or more strings to hash
* `string|cdata Cipher:digest(returnType)` - Finishes and returns the hash in one of three formats
	* `hex` encodes it in hexadecimal
	* `string` gives the raw bytes in a Lua string
	* `cdata` gives the plain `unsigned char[]` from the FFI lib
* `Cipher.size` - Size of the cipher in bytes, `MD5` -> `16`, `SHA224` -> `28`, `SHA256` -> `32`, etc

# Example

```lua
local hash = require("openssh-hash")

print(hash.cipher("MD5"):update("hello"):digest("hex")) -->
	-- 5d41402abc4b2a76b9719d911017c592

print(hash.cipher("SHA224"):update("hel"):update("lo"):digest("hex")) -->
	-- ea09ae9cc6768c50fcee903ed054556e5bfc8347907f12598aa24193

print(hash.cipher("SHA256"):update("hel", "lo"):digest("hex")) -->
	-- 2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824
```

