local hash = require("openssh-hash")

p(hash.cipher("SHA256"):update("sæll"):digest("hex"))
p(hash.cipher("MD5"):update("sæll"):digest("string"))
p(hash.cipher("SHA224"):update("sæll"):digest("cdata"))

assert(hash.cipher("MD5"):update(""):update("sæll"):digest("string") == hash.cipher("MD5"):update("sæll"):update(""):digest("string"))
assert(hash.cipher("MD5"):update(""):digest("hex")                   == "d41d8cd98f00b204e9800998ecf8427e")
assert(hash.cipher("SHA256"):update("hello"):digest("hex")           == "2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824")

local function shouldFail(func)
	local ok, err = pcall(func)
	assert(not ok)
	print(err)
end


shouldFail(function() hash.cipher(":D") end)

local h = hash.cipher("SHA512")
shouldFail(function() h:update(nil) end)
shouldFail(function() h:update(5) end)
shouldFail(function() h:update("he", 6) end)
shouldFail(function() h:update(":D"):digest(nil) end)
shouldFail(function() h:update(":D"):digest("hex") end)


