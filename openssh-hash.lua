---
-- @module openssh-hash
-- @license MPL-v2
-- @author nazrin
local ffi = require("ffi")
local ssl = ffi.load("ssl")
if not ssl then error("Failed to load ssl library") end

local hash = { _LICENSE = "MPL-v2", _VERSION = "1.0.0" }

-- https://www.openssl.org/docs/man1.1.1/man3/EVP_MD_CTX_new.html
ffi.cdef("int   EVP_DigestFinal_ex(void*, unsigned char*, unsigned int*)")
ffi.cdef("int   EVP_DigestInit_ex(void*, void*, void*)")
ffi.cdef("int   EVP_DigestUpdate(void*, const unsigned char*, size_t)")
ffi.cdef("int   EVP_MD_size(void* md);")
ffi.cdef("void  EVP_MD_CTX_free(void*)")
ffi.cdef("void* EVP_MD_CTX_new()")
ffi.cdef("void* EVP_get_digestbyname(const char*)")

local Cipher = {}
Cipher.__index = Cipher
--- Updates hash
-- @string name
-- @string[opt=nil] ...
function Cipher:update(...)
	assert(self.ctx, "Hash finished already")
	local strs = {...}
	assert(strs[1], "Need at least one string")
	for i,s in ipairs(strs) do
		assert(type(s) == "string", ("Arg %d is not a string: %q"):format(i, tostring(s)))
		ssl.EVP_DigestUpdate(self.ctx, s, #s)
	end
	return self
end
--- Finishes and returns hash
-- @string returnType "hex" "string" "cdata"
-- @return[1] string hex
-- @return[2] string raw bytes
-- @return[3] cdata FFI unsigned char[]
function Cipher:digest(typ)
	assert(self.ctx, "Hash finished already")
	local val = ffi.new("unsigned char[?]", self.size)
	ssl.EVP_DigestFinal_ex(self.ctx, val, nil)
	self.ctx = nil
	local ret
	if typ == "hex" then
		local h = {}
		for i=0,self.size-1 do
			h[i+1] = string.format("%02x", val[i])
		end
		ret = table.concat(h, "")
	elseif typ == "string" then
		ret = ffi.string(val, self.size)
	elseif typ == "cdata" then
		ret = val
	else
		error(("Invalid returnType %q"):format(typ))
	end
	return ret
end

--- Creates a cipher CTX
-- @string name
-- @return Cipher
function hash.cipher(name)
	local md = ssl.EVP_get_digestbyname(name)
	if md == nil then error(("Failed to get digest pointer %q"):format(name)) end
	local ctx = ssl.EVP_MD_CTX_new()
	if not ctx then error("Failed to create ctx") end
	ffi.gc(ctx, ssl.EVP_MD_CTX_free)
	ssl.EVP_DigestInit_ex(ctx, md, nil)
	local size = ssl.EVP_MD_size(md)
	return setmetatable({ ctx = ctx, size = size }, Cipher)
end

return hash

